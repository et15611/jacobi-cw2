CC = icc
LD = xild
AR = xiar
#CFLAGS = -std=c99 -Wall -O2 -ffast-math #-ipo
CFLAGS = -std=c99 -Wall -O2 -ipo -fast -xHost -qopenmp -vec-report=2 
LDFLAGS = -lm 

jacobi: jacobi.c
	$(CC) $(CFLAGS) -o jacobi jacobi.c $(LDFLAGS)

jacobi-m: jacobi.c #merged, non-parallel initialisation
	  $(CC) $(CFLAGS) -o jacobim jacobi.c $(LDFLAGS)

jacobi-u: jacobi.c #unmerged, non-parallel initialisation
	  $(CC) $(CFLAGS) -o jacobiu jacobi.c $(LDFLAGS)

jacobi-iu: jacobi.c #initialised in parallel, unmerged
	   $(CC) $(CFLAGS) -o jacobiiu jacobi.c $(LDFLAGS)

jacobi-im: jacobi.c #initialised in parallel, merged
	   $(CC) $(CFLAGS) -o jacobiim jacobi.c $(LDFLAGS)

jacobi-ims: jacobi.c #initialised in parallel, merged
	    $(CC) $(CFLAGS) -o jacobiims jacobi.c $(LDFLAGS)

jacobi-ius: jacobi.c #initialised in parallel, merged
	    $(CC) $(CFLAGS) -o jacobiius jacobi.c $(LDFLAGS)

jacobi-ima: jacobi.c #initialised in parallel, merged
	    $(CC) $(CFLAGS) -o jacobiima jacobi.c $(LDFLAGS)

jacobi-iua: jacobi.c #initialised in parallel, merged
	    $(CC) $(CFLAGS) -o jacobiiua jacobi.c $(LDFLAGS)

jacobi-serial: jacobiserial.c #initialised in parallel, merged
	    $(CC) $(CFLAGS) -o jacobiserialm jacobiserial.c $(LDFLAGS)

testing: jacobi.c
	 $(CC) $(CFLAGS) -o jacobitest jacobi.c $(LDFLAGS)
